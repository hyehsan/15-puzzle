FROM node:17-alpine
WORKDIR /usr/src/app
COPY package.json ./
COPY package-lock.json ./
RUN --mount=type=cache,target=/usr/src/app/.npm \
    npm set cache /usr/src/app/.npm && \
    npm ci
COPY ./ ./
RUN npm run build
RUN ["chmod", "+x", "/usr/src/app/docker-entrypoint.sh"]
ENTRYPOINT ["/usr/src/app/docker-entrypoint.sh"]
CMD ["npm", "run", "start"]
